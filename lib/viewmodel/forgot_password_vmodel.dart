import 'package:app_one/locator/locator.dart';
import 'package:app_one/model/response/forgot_password.dart';
import 'package:app_one/model/response/update_password.dart';
import 'package:app_one/services/apis/api_services.dart';
import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/viewmodel/base_model.dart';

class ForgotPasswordViewModel extends BaseModel {

  ApiServices apiService = locator<ApiServices>();
  ForgotPassword forgotPasswordData;
  bool forgotPasswordSuccess;
  String forgotPasswordErrorMsg;
  UpdatePassword updatePasswordData;
  bool updatePasswordSuccess;
  String updatePasswordErrorMsg;

  Future<bool> forgotPassword(parameterMap)async{
    var result=await apiService.apiRequestForgotPassword(parameterMap);
    if(result[0]!='')
    {
      forgotPasswordData =result[0];
      forgotPasswordSuccess = true;

    }
    else{
      forgotPasswordErrorMsg = result[1];
      forgotPasswordSuccess = false;
    }
    return forgotPasswordSuccess;
  }


  Future<bool> updatePassword(parameterMap)async{
    var result=await apiService.apiRequestUpdatePassword(parameterMap);
    if(result[0]!='')
    {
      updatePasswordData =result[0];
      updatePasswordSuccess = true;

    }
    else{
      updatePasswordErrorMsg = result[1];
      updatePasswordSuccess = false;
    }
    return updatePasswordSuccess;
  }
}