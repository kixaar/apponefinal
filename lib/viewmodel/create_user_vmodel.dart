import 'package:app_one/locator/locator.dart';
import 'package:app_one/model/response/countries.dart';
import 'package:app_one/model/response/register_user.dart';
import 'package:app_one/services/apis/api_services.dart';
import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/viewmodel/base_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateUserViewModel extends BaseModel {

  FireBaseService fireBaseService = locator<FireBaseService>();
  ApiServices apiService = locator<ApiServices>();
  RegisterUser registrationData;
  bool registerSuccess;
  String registrationErrorMsg;
  Countries countryData;
  var countriesErrorMsg;
  bool countrySuccess;
  var cityData;
  bool citySuccess;
  var cityErrorMsg;
  var businessProfileData;
  bool businessProfileSuccess;
  var businessProfileErrorMsg;

  Future<String> createUser({email,password,userImageFile}) async {
    return await fireBaseService.createUserWithEmailPass(email: email,password: password,
        userImageFile: userImageFile);
  }

  Future<bool> updateUserFcmToken()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    var update=false;
    var tokenResult =await fireBaseService.getToken();
    if(tokenResult != 'Failed')
      {
        await FirebaseFirestore.instance.collection("users")
            .doc(FireBaseService.getCurrentUserUid()).update({
          'fcmToken':tokenResult
        }).then((value) => {
          pref.setString('userFcmToken', tokenResult),
         print("Login: Token Update Successful $tokenResult"),
         update = true
        }).catchError((error)=>{
          print(error),
          update = false
        });
      }
    else{
      update = false;
    }
   return update;
  }

  Future<bool> registerUser(parameterMap)async{
     var result=await apiService.apiRequestRegisterUser(parameterMap);
     if(result[0]!='')
       {
         registrationData =result[0];
         registerSuccess = true;

       }
     else{
       registrationErrorMsg = result[1];
       registerSuccess = false;
     }
     return registerSuccess;
  }

  Future<bool> getCountries()async{
    var result=await apiService.apiRequestGetCountries();
    if(result[0]!='')
    {
      countryData =result[0];
      countrySuccess = true;

    }
    else{
      countriesErrorMsg = result[1];
      countrySuccess = false;
    }
    return countrySuccess;
  }

  Future<bool> getCities(String countryId)async{
    var result=await apiService.apiRequestGetCities(countryId);
    if(result[0]!='')
    {
      cityData =result[0];
      citySuccess = true;

    }
    else{
      cityErrorMsg = result[1];
      citySuccess = false;
    }
    return citySuccess;
  }

  Future<bool> createBusinessProfile(parameterMap)async{
    var result=await apiService.apiRequestCreateBusinessProfile(parameterMap);
    if(result[0]!='')
    {
      businessProfileData =result[0];
      businessProfileSuccess = true;

    }
    else{
      businessProfileErrorMsg = result[1];
      businessProfileSuccess = false;
    }
    return businessProfileSuccess;
  }
}