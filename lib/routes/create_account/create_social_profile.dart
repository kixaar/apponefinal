import 'dart:io';
import 'dart:convert';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/globals/globals.dart';
import 'package:app_one/model/response/interests.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/image_picker/image_picker.dart';
import 'package:app_one/viewmodel/create_user_vmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_tag_editor/tag_editor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateSocialProfile extends StatefulWidget {
  final Map<String,dynamic> userRegistrationData;

  CreateSocialProfile({this.userRegistrationData});

  @override
  _CreateSocialProfileScreenState createState() => _CreateSocialProfileScreenState();
}


class _CreateSocialProfileScreenState extends State<CreateSocialProfile> {
  var _valueProfileType;
  List<Interests> _listOfInterestsChipTags = [];
  PickedFile imageFile;
  TextEditingController profileNameTC = TextEditingController();
  TextEditingController aboutTC = TextEditingController();
  Map<String,dynamic> userRegistrationData;

  @override
  void initState() {
    userRegistrationData = widget.userRegistrationData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  BaseView<CreateUserViewModel>(
      builder: (context, model, child) => createSocialAccountView(context, model),
    );
  }

  Scaffold createSocialAccountView(BuildContext context,CreateUserViewModel model)  {
    return Scaffold(
      backgroundColor: MyColors.colorDarkBlack,
      body: BaseScrollView().baseView(context, [

        Padding(
          padding: EdgeInsets.only(top: 70,left: 15,right: 15,bottom: 30),
          child:profileItem(
              ()async{
                imageFile = await PickImageController.instance.picker
                    .getImage(source: ImageSource.gallery,imageQuality: 30);
                if (imageFile != null) {
                  print("${File(imageFile.path).lengthSync() * 0.001} Kb");
                  setState(() {
                    imageFile;
                  });
                }
              }
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40,left: 50,right: 50),
          child: plainTextField(Localization.stLocalized('profileName'),
          profileNameTC),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Text(
            Localization.stLocalized('aboutMe'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 30,left:50,right: 50 ),
          child: whiteMultilineTextField(aboutTC),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Text(
            Localization.stLocalized('profileType'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(left: 20,right: 20),
          child: Stack(
            children: [
              Wrap(
                direction: Axis.horizontal,
                children: [
                  SizedBox(
                    width: 145,
                    child: Theme(
                      data: ThemeData(
                        unselectedWidgetColor: MyColors.colorLogoOrange,
                      ),
                      child: RadioListTile(
                        activeColor: MyColors.colorLogoOrange,
                          groupValue: _valueProfileType,
                          value: "public",
                          onChanged: (value)=>{
                            setState((){
                              _valueProfileType = value;
                            })
                          },
                          title: Text(
                            "Public",
                            style: CTheme.textRegular16White(),
                          ),
                        ),
                    ),
                  ),

                   SizedBox(
                     width: 145,
                     child: Theme(
                       data: ThemeData(
                         unselectedWidgetColor: MyColors.colorLogoOrange
                       ),
                       child: RadioListTile(
                         activeColor: MyColors.colorLogoOrange,
                          groupValue: _valueProfileType,
                          value: "private",
                          onChanged: (value)=>{
                            setState((){
                              _valueProfileType = value;
                            })
                          },
                          title: Text(
                            "Private",
                            style: CTheme.textRegular16White(),
                          ),
                        ),
                     ),
                   ),
                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Text(
            Localization.stLocalized('selectInterests'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top:15,left: 40,right: 40,bottom: 30),
          child:Container(
            decoration: BoxDecoration(
                color: MyColors.colorWhite,
                borderRadius: BorderRadius.circular(8)
            ),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: TagEditor(
                enableSuggestions: true,
                length: _listOfInterestsChipTags.length,
                delimeters: [',', ' '],
                hasAddButton: false,
                inputDecoration: const InputDecoration(

                  contentPadding:EdgeInsets.only(left: 5) ,
                  border: InputBorder.none,
                  hintText: 'Enter interests, separate by pressing space',
                ),
                onTagChanged: (newValue) {
                  setState(() {
                    _listOfInterestsChipTags.add(Interests(id: null,name: newValue));
                  });
                },
                tagBuilder: (context, index) => _Chip(
                  index: index,
                  label: _listOfInterestsChipTags[index].name,
                  onDeleted: (index)=>{
                    setState((){
                      _listOfInterestsChipTags.removeAt(index);
                    })
                  },
                ),
              )
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40,left: 50,right: 50,bottom: 30),
          child: roundedSquareButton(Localization
              .stLocalized('createSocialProfile'),50,
                  ()async
                  {
            if(validateFields()){
              userRegistrationData['profile_name']=profileNameTC.text.trim();
              userRegistrationData['about']=aboutTC.text.trim();
              userRegistrationData['profile_about']=aboutTC.text.trim();
              userRegistrationData['interest']= [
                {"id":1,"name":"Showbiz"}
              ];
              userRegistrationData['profile_type']="social";
              userRegistrationData['profile_status']=_valueProfileType;
              userRegistrationData['profile_image']= await convertImageToBase64(imageFile);
              CTheme.showCircularProgressDialog(context);
              createUserInFirebase(userRegistrationData['email'],
                  userRegistrationData['password'],
                  model);
              }
            }),
          )
        ])
  );
  }

  Widget profileItem(Function onTap) {
    return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Card(
                  color: MyColors.appBlue,
                  elevation: 8,
                  shadowColor: MyColors.appBlue,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: imageFile==null
                            ?Container(
                          height: 140,
                          width: 140,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: MyColors.colorWhite,
                            border: Border.all(
                              color: MyColors.colorLogoOrange,
                              width: 6
                            )
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(2),
                            child: Text(
                              Localization.stLocalized('profileImage'),
                              style: CTheme.textRegular25LogoOrange(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                            :Container(
                            height: 140,
                            width: 140,
                            child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(75)),
                                child: Image.file(
                                  File(imageFile.path),
                                  fit: BoxFit.cover,)
                            )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15,left: 85,right: 85,bottom: 20),
                        child: roundedSquareButton(Localization
                            .stLocalized('uploadImage'),
                            30 ,onTap),
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
  }

  Container whiteMultilineTextField(TextEditingController controller) {
    return Container(
            decoration: BoxDecoration(
              color: MyColors.colorWhite,
              borderRadius: BorderRadius.circular(8)
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 10,right: 10),
              child: TextField(
                controller: controller,
                maxLines: 4,
                style: CTheme.textRegular16Black(),
                decoration: InputDecoration(
                  border: UnderlineInputBorder(
                    borderSide: BorderSide.none
                  ),
                  fillColor: MyColors.colorWhite,
                ),
              ),
            ),
          );
  }

  Widget profileTypeBox(String headingText,String subText,String btnText
      ,Function onTap)
  {
    return Container(
            decoration: BoxDecoration(
              color: MyColors.appBlue,
              border: Border(
                left: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                right: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                bottom: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                top: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Text(
                    headingText,
                    style: CTheme.textRegular26LogoOrange(),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                      height: 0.5,
                      width: 200,
                      color: MyColors.colorLogoOrange,
                    ),
                  ),

                Padding(
                  padding: const EdgeInsets.only(top:10,left: 75,right: 75),
                  child: Text(
                    subText,
                    style: CTheme.textRegular11WhiteItalic(),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 25,left: 40,right: 40,bottom: 25),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: onTap,
                          child: Container(
                            alignment: Alignment.center,
                            height: 50,
                            decoration: (
                                BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                        Color(0xFF242A37),
                                        Color(0xFF4E586E)
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(1.0, 0.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                )
                            ),
                            child: Text(
                              btnText,
                              style: CTheme.textRegularBold18White(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                )
              ],
            ),
          );
  }

  Widget roundedSquareButton(String btnText,double height,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: height,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText,TextEditingController controller) {
    return TextField(
      controller: controller,
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }

  bool validateFields(){
    var isValidated = false;
    if(imageFile == null){
      CTheme.showAppAlertOneButton(context: context,title: 'Image Error',
          bodyText: 'Please select profile image to proceed',btnTitle: 'Okay');
    }
    else if(profileNameTC.text.trim().length==0){
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please specify profile name',btnTitle: 'Okay');
    }
    else if(_valueProfileType == null){
      CTheme.showAppAlertOneButton(context: context,title: 'Select Profile Type',
          bodyText: 'Please select type of profile',
          btnTitle: 'Okay');
    }
    ///List of tags commented for now.
    // else if(_listOfInterestsChipTags.isEmpty){
    //   CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
    //       bodyText: 'Please select interests to proceed',
    //       btnTitle: 'Okay');
    // }

    else{
      isValidated = true;
    }

    return isValidated;
  }

  createUserInFirebase(email,password,CreateUserViewModel model)async {
    String signUpResult = await model.createUser(
        email: email,
        password: password,
        userImageFile: File(imageFile.path));
    if(signUpResult == 'Successful')
      {
        registerUserInApi(model);
      }
    else{
      Navigator.pop(context);
      CTheme.showAppAlertOneButton(
          context: context,
          title: 'Error SigningUp',
          bodyText: signUpResult);
    }
  }

  registerUserInApi(CreateUserViewModel model) async {
    await model.registerUser(userRegistrationData);
    if(model.registerSuccess) {
      Navigator.pop(context);
      print("Successful");
      print(model.registrationData.data.email);
      await addValueToSharedPreference(model);
      CTheme.showAppAlertTwoButton(context: context,title: "Business Account",
        btn1Title: 'Create',btn2Title: 'Continue',
        bodyText: "Social Account created successfully do you want to create Business Account?",
        handler1: (handler)=>{
        Navigator.pushNamed(context,'/create_business_profile')
        },
        handler2: (handler)=>{
        Globals.currentRoute=='/profile_home',
        Navigator.pushNamedAndRemoveUntil(context, '/profile_home',
        (route)=>false)
        }
      );

    } else {
      deleteUserFromDatabase();
      Navigator.pop(context);
      print("Error Register User Api");
      CTheme.showAppAlertOneButton(
          context: context,
          title: 'Error SigningUp',
          bodyText: "The was some error creating profile try later.");
    }
  }

  Future addValueToSharedPreference(CreateUserViewModel model) async {
      await SharedPreferences.getInstance().then((value) => {
      value.setString(Globals.userAuthToken, model.registrationData.data.token),
        value.setString(Globals.userName, model.registrationData.data.name),
        value.setString(Globals.profileId, model.registrationData.data.id.toString()),
        value.setString(Globals.userId, model.registrationData.data.userId.toString()),
        value.setString(Globals.profileType, model.registrationData.data.profileType.toString()),
        value.setString(Globals.profileStatus, model.registrationData.data.profileStatus.toString())

      });
  }

  void deleteUserFromDatabase() {
    ///Todo
  }

  Future<String> convertImageToBase64(imageFile)async{
    var headerString = 'data:image/png;base64,';
    File image = new File(imageFile.path);
    List<int> imageBytes = await image.readAsBytesSync();
    String base64Image = await base64.encode(imageBytes);
    var encodedImage ="$headerString$base64Image";
    print("Base64 Image Content: $encodedImage");
    return encodedImage;
  }
}

class _Chip extends StatelessWidget {
  const _Chip({
    @required this.label,
    @required this.onDeleted,
    @required this.index,
  });

  final String label;
  final ValueChanged<int> onDeleted;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: MyColors.colorLogoOrange,
      labelPadding: const EdgeInsets.only(left: 8.0),
      label: Text(label),
      deleteIcon: Icon(
        Icons.close,
        size: 15,
        color: MyColors.colorWhite,
      ),
      onDeleted: () {
        onDeleted(index);
      },
    );
  }
}