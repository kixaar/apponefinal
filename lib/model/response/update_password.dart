/// status_code : 200
/// message : "Your password has been updated"

class UpdatePassword {
  int _statusCode;
  String _message;

  int get statusCode => _statusCode;
  String get message => _message;

  UpdatePassword({
      int statusCode, 
      String message}){
    _statusCode = statusCode;
    _message = message;
}

  UpdatePassword.fromJson(dynamic json) {
    _statusCode = json["status_code"];
    _message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status_code"] = _statusCode;
    map["message"] = _message;
    return map;
  }

}