/// status_code : 200
/// data : {"id":20,"user_id":12,"name":"first social","email":"socialuser1@gmail.com","profile_type":"social","profile_status":"public","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOWE4N2FjNDU3NGQ2MjNhMWRiYmJiMDc0NzMxZjg5ZWEwMmE1MWRhNTM1Zjg5YTY0NjkxNzQxMzQ1ZmRhNDlkODdiM2IzNmMwMDBjMjQ0ZTQiLCJpYXQiOjE2MDc1MTU1NDMsIm5iZiI6MTYwNzUxNTU0MywiZXhwIjoxNjM5MDUxNTQzLCJzdWIiOiIxMiIsInNjb3BlcyI6W119.hh-upo6Dy-0QdXB4EaYwJZccNk3zTZ8vzheHwctpP_-x563iME9amVlel0x0uDJRTe6xP-fsCCa0fhrFPxun6awg9YcDu48VN_jU6g89HOO4omY3hkBp_cvmw9on-eNnPB0oJwiMb25XxqXJAyKsnGnwQPuQ4BKBI5WjbFx6EK7ReZreEh7TB3c8vv-HRZVF2aYJNLictqzLYx8DkhToiXJgNbhkWCBJt9ohQuZiniQEMBMctFEX_SFLVJTtLZdkbxIGRYtBkfsjnjql8WOzSmsJpRsmzr6v2w6iEklpGzNxFhEkbTpkwyXun7LFsdRbIegsZ62YoefY2BU-IqPORqvZV2yv7sfHViCzIeXOH01yOz-5WA3QWq2_Of7W99KscQZqJSaHvM9OxbKG5ZqHfFKJYUsQpD-J_kA9r1QyQm8uiC9leLLsQ0Hnfg9MqesYbknDqGzP30dhtpranQtTGhljMsj_XEDBLvYjkeYWPO7j9kf8DNo-RQHuNPsQByS_7BYBh5V7lxB_u7ZstHh2RjhN6pc7ZKJaqGTs5Rw6XU7Np0XI9V46x4I-WZ2DxYSV2mjfDAJcLb2b52x0jFTFTT4PNWb-W2Md3rcvuKDf4Nc-ppwSeVRruzeHxjRHucyufBVc8Ks7BRoHWXxQOkoyo4B42xn0-2jjXVKHzs5zP5o"}

class Login {
  int _statusCode;
  Data _data;

  int get statusCode => _statusCode;
  Data get data => _data;

  Login({
      int statusCode, 
      Data data}){
    _statusCode = statusCode;
    _data = data;
}

  Login.fromJson(dynamic json) {
    _statusCode = json["status_code"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status_code"] = _statusCode;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// id : 20
/// user_id : 12
/// name : "first social"
/// email : "socialuser1@gmail.com"
/// profile_type : "social"
/// profile_status : "public"
/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOWE4N2FjNDU3NGQ2MjNhMWRiYmJiMDc0NzMxZjg5ZWEwMmE1MWRhNTM1Zjg5YTY0NjkxNzQxMzQ1ZmRhNDlkODdiM2IzNmMwMDBjMjQ0ZTQiLCJpYXQiOjE2MDc1MTU1NDMsIm5iZiI6MTYwNzUxNTU0MywiZXhwIjoxNjM5MDUxNTQzLCJzdWIiOiIxMiIsInNjb3BlcyI6W119.hh-upo6Dy-0QdXB4EaYwJZccNk3zTZ8vzheHwctpP_-x563iME9amVlel0x0uDJRTe6xP-fsCCa0fhrFPxun6awg9YcDu48VN_jU6g89HOO4omY3hkBp_cvmw9on-eNnPB0oJwiMb25XxqXJAyKsnGnwQPuQ4BKBI5WjbFx6EK7ReZreEh7TB3c8vv-HRZVF2aYJNLictqzLYx8DkhToiXJgNbhkWCBJt9ohQuZiniQEMBMctFEX_SFLVJTtLZdkbxIGRYtBkfsjnjql8WOzSmsJpRsmzr6v2w6iEklpGzNxFhEkbTpkwyXun7LFsdRbIegsZ62YoefY2BU-IqPORqvZV2yv7sfHViCzIeXOH01yOz-5WA3QWq2_Of7W99KscQZqJSaHvM9OxbKG5ZqHfFKJYUsQpD-J_kA9r1QyQm8uiC9leLLsQ0Hnfg9MqesYbknDqGzP30dhtpranQtTGhljMsj_XEDBLvYjkeYWPO7j9kf8DNo-RQHuNPsQByS_7BYBh5V7lxB_u7ZstHh2RjhN6pc7ZKJaqGTs5Rw6XU7Np0XI9V46x4I-WZ2DxYSV2mjfDAJcLb2b52x0jFTFTT4PNWb-W2Md3rcvuKDf4Nc-ppwSeVRruzeHxjRHucyufBVc8Ks7BRoHWXxQOkoyo4B42xn0-2jjXVKHzs5zP5o"

class Data {
  int _id;
  int _userId;
  String _name;
  String _email;
  String _profileType;
  String _profileStatus;
  String _token;

  int get id => _id;
  int get userId => _userId;
  String get name => _name;
  String get email => _email;
  String get profileType => _profileType;
  String get profileStatus => _profileStatus;
  String get token => _token;

  Data({
      int id, 
      int userId, 
      String name, 
      String email, 
      String profileType, 
      String profileStatus, 
      String token}){
    _id = id;
    _userId = userId;
    _name = name;
    _email = email;
    _profileType = profileType;
    _profileStatus = profileStatus;
    _token = token;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _userId = json["user_id"];
    _name = json["name"];
    _email = json["email"];
    _profileType = json["profile_type"];
    _profileStatus = json["profile_status"];
    _token = json["token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user_id"] = _userId;
    map["name"] = _name;
    map["email"] = _email;
    map["profile_type"] = _profileType;
    map["profile_status"] = _profileStatus;
    map["token"] = _token;
    return map;
  }

}