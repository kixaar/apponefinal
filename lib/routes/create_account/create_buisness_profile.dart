import 'dart:convert';
import 'dart:io';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/globals/globals.dart';
import 'package:app_one/model/response/cities.dart';
import 'package:app_one/model/response/countries.dart';
import 'package:app_one/model/response/interests.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/image_picker/image_picker.dart';
import 'package:app_one/viewmodel/create_user_vmodel.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_tag_editor/tag_editor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateBusinessProfile extends StatefulWidget {

  @override
  _CreateBusinessProfileScreenState createState() => _CreateBusinessProfileScreenState();
}


class _CreateBusinessProfileScreenState extends State<CreateBusinessProfile> {
  var radioValue;
  TextEditingController textControllerBN=TextEditingController();
  List<String> listOfCountries=[];
  PickedFile imageFile;
  List<Interests> _listOfInterestsChipTags = [];
  TextEditingController businessNameTC= TextEditingController();
  TextEditingController emailTC= TextEditingController();
  TextEditingController businessWebsiteTC= TextEditingController();
  TextEditingController aboutTC= TextEditingController();
  List<String> listOfCities=[];
  String _valueCountry;
  String _valueCity;
  var _valueProfileType;



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<CreateUserViewModel>(
      onModelReady: (model) async {
        CTheme.showCircularProgressDialog(context);
        await model.getCountries();
        if(model.countrySuccess)
        {
          print("Successful");
          Navigator.pop(context);
          populateCountryListForDropdown(model.countryData);
          await getCitiesByCountryId(
              model,
              '202');
          setState(() {
            listOfCountries;
            _valueCountry='202-South Africa';
            _valueCity='38028-Alberton';
          });
        }
        else{
          Navigator.pop(context);
          print("Error");
        }
      },
      builder: (context, model, child) => createBusinessProfileView(context, model),
    );
  }

  void populateCountryListForDropdown(Countries countryData) {
    countryData.data.forEach((element) {
      listOfCountries.add("${element.id}-${element.name}");
    });
  }

  Scaffold createBusinessProfileView(BuildContext context,CreateUserViewModel model) {
    return Scaffold(
      backgroundColor: MyColors.appBlue,
      body: BaseScrollView().baseView(context, [

        Padding(
          padding: EdgeInsets.only(top: 70,left: 15,right: 15,bottom: 30),
          child:profileItem(
                  ()async{
                imageFile = await PickImageController.instance.picker
                    .getImage(source: ImageSource.gallery,imageQuality: 30);
                if (imageFile != null) {
                  print("${File(imageFile.path).lengthSync() * 0.001} Kb");
                  setState(() {
                    imageFile;
                  });
                }
              }
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40,left: 50,right: 50),
          child: plainTextField(Localization.stLocalized('businessName'),
          businessNameTC),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30,left: 50,right: 50),
          child: plainTextField(Localization.stLocalized('emailUsed'),
              emailTC),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30,left: 50,right: 50),
          child: plainTextField(Localization.stLocalized('businessWebsite'),
              businessWebsiteTC),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30,left: 35,right: 35),
          child: Container(
            height: 50,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Theme(
                data: Theme.of(context).copyWith(
                    canvasColor: MyColors.appBlue),
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<String>(
                    icon: Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.arrow_drop_down,
                        size: 25,
                      ),
                    ),
                    underline: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Container(
                        color: MyColors.colorWhite,
                        height: 1,
                      ),
                    ),
                    iconEnabledColor: Colors.white,
                    iconDisabledColor: Colors.white,
                    hint: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "Country",
                            style: CTheme.textRegular18White(),
                          ),
                        ),
                      ],
                    ),

                    isExpanded: true,
                    items: listOfCountries.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value.substring(
                              value.indexOf("-") + 1, value.length),
                          style: CTheme.textRegular18White(),
                          textAlign: TextAlign.start,
                        ),
                      );
                    }).toList(),
                    onChanged: (value) {
                      getCitiesByCountryId(
                          model,
                          value.substring(0, value.indexOf('-')));
                      setState(() {
                        _valueCountry = value;
                      });
                    },
                    value: _valueCountry,
                  ),
                ),
              ),
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30,left: 35,right: 35),
          child: Container(
            height: 50,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Theme(
                data: Theme.of(context).copyWith(
                    canvasColor: MyColors.appBlue),
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<String>(
                    icon: Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.arrow_drop_down,
                        size: 25,
                      ),
                    ),
                    underline: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Container(
                        color: MyColors.colorWhite,
                        height: 1,
                      ),
                    ),
                    iconEnabledColor: Colors.white,
                    iconDisabledColor: Colors.white,
                    hint: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "City",
                            style: CTheme.textRegular18White(),
                          ),
                        ),
                      ],
                    ),

                    isExpanded: true,
                    items: listOfCities.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value.substring(
                              value.indexOf("-") + 1, value.length),
                          style: CTheme.textRegular18White(),
                          textAlign: TextAlign.start,
                        ),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _valueCity = value;
                      });
                    },
                    value: _valueCity,
                  ),
                ),
              ),
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Text(
            Localization.stLocalized('aboutMe'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 30,left:50,right: 50 ),
          child: whiteMultilineTextField(),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Text(
            Localization.stLocalized('profileType'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(left: 20,right: 20),
          child: Stack(
            children: [
              Wrap(
                direction: Axis.horizontal,
                children: [
                  SizedBox(
                    width: 138,
                    child: Theme(
                      data: ThemeData(
                        unselectedWidgetColor: MyColors.colorLogoOrange,
                      ),
                      child: RadioListTile(
                        activeColor: MyColors.colorLogoOrange,
                        groupValue: _valueProfileType,
                        value: "public",
                        onChanged: (value)=>{
                          setState((){
                            _valueProfileType = value;
                          })
                        },
                        title: Text(
                          "Public",
                          style: CTheme.textRegular16White(),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    width: 138,
                    child: Theme(
                      data: ThemeData(
                          unselectedWidgetColor: MyColors.colorLogoOrange
                      ),
                      child: RadioListTile(
                        activeColor: MyColors.colorLogoOrange,
                        groupValue: _valueProfileType,
                        value: "private",
                        onChanged: (value)=>{
                          setState((){
                            _valueProfileType = value;
                          })
                        },
                        title: Text(
                          "Private",
                          style: CTheme.textRegular16White(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Text(
            Localization.stLocalized('selectInterests'),
            style: CTheme.textRegular21White(),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top:15,left: 40,right: 40,bottom: 30),
          child:Container(
            decoration: BoxDecoration(
                color: MyColors.colorWhite,
                borderRadius: BorderRadius.circular(8)
            ),
            child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: TagEditor(
                  enableSuggestions: true,
                  length: _listOfInterestsChipTags.length,
                  delimeters: [',', ' '],
                  hasAddButton: false,
                  inputDecoration: const InputDecoration(

                    contentPadding:EdgeInsets.only(left: 5) ,
                    border: InputBorder.none,
                    hintText: 'Enter interests, separate by pressing space',
                  ),
                  onTagChanged: (newValue) {
                    setState(() {
                      _listOfInterestsChipTags.add(Interests(id: null,name: newValue));
                    });
                  },
                  tagBuilder: (context, index) => _Chip(
                    index: index,
                    label: _listOfInterestsChipTags[index].name,
                    onDeleted: (index)=>{
                      setState((){
                        _listOfInterestsChipTags.removeAt(index);
                      })
                    },
                  ),
                )
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 40,left: 50,right: 50,bottom: 30),
          child: roundedSquareButton(Localization
              .stLocalized('createBusinessProfile'),50,
                  ()async{
            if(validateFields())
              {
                CTheme.showCircularProgressDialog(context);
                SharedPreferences prefs = await SharedPreferences.getInstance();
                await model.createBusinessProfile(
                    {
                      "profile_image": await convertImageToBase64(imageFile),
                      "user_id": prefs.get(Globals.userId),
                      "country_id": _valueCountry.substring(0,_valueCountry.indexOf('-')),
                      "city_id": _valueCity.substring(0,_valueCity.indexOf('-')),
                      "profile_name":businessNameTC.text.trim(),
                      "profile_email": emailTC.text.trim(),
                      "profile_phone": "",
                      "profile_address": "",
                      "profile_website": businessWebsiteTC.text.trim(),
                      "profile_about": aboutTC.text.trim(),
                      "profile_type": "business",
                      "profile_status": _valueProfileType,
                      "about": aboutTC.text.trim(),
                      "interest":[
                        {"id": 1,
                        "name": "Showbiz",}
                        ]
                    }
                    );
                if(model.businessProfileSuccess)
                  {
                    Navigator.pop(context);
                    Navigator.pushNamedAndRemoveUntil(context, '/profile_home',
                        (route)=>false);
                    print("Created Successfully");
                  }
                else
                  {
                    Navigator.pop(context);
                    print("Error");
                  }
              }

          }),
        )

      ])
  );
  }

  Future<String> convertImageToBase64(imageFile)async{
    var headerString = 'data:image/png;base64,';
    File image = new File(imageFile.path);
    List<int> imageBytes = await image.readAsBytesSync();
    String base64Image = await base64.encode(imageBytes);
    var encodedImage ="$headerString$base64Image";
    print("Base64 Image Content: $encodedImage");
    return encodedImage;
  }

  void getCitiesByCountryId(CreateUserViewModel model,String countryId)async {
    CTheme.showCircularProgressDialog(context);
    await model.getCities(countryId);
    if(model.citySuccess)
    {
      print("Successful");
      populateCityListForDropdown(model.cityData);
      setState(() {
        listOfCities;
      });
      Navigator.pop(context);
    }
    else{
      Navigator.pop(context);
      print("Error");
    }
  }

  void populateCityListForDropdown(Cities cityData) {
    listOfCities=[];
    cityData.data.forEach((element) {
      listOfCities.add("${element.id}-${element.name}");
    });
    listOfCities.sort((a, b) {
      return a.substring(a.indexOf("-")+1).toLowerCase()
          .compareTo(b.substring(b.indexOf("-")+1).toLowerCase());
    });
    print(listOfCities);
  }

  Widget profileItem(Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Card(
            color: MyColors.appBlue,
            elevation: 8,
            shadowColor: MyColors.appBlue,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: imageFile==null
                      ?Container(
                    height: 140,
                    width: 140,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: MyColors.colorWhite,
                        border: Border.all(
                            color: MyColors.colorLogoOrange,
                            width: 6
                        )
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(2),
                      child: Text(
                        Localization.stLocalized('profileImage'),
                        style: CTheme.textRegular25LogoOrange(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                      :Container(
                      height: 140,
                      width: 140,
                      child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(75)),
                          child: Image.file(
                            File(imageFile.path),
                            fit: BoxFit.cover,)
                      )
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15,left: 85,right: 85,bottom: 20),
                  child: roundedSquareButton(Localization
                      .stLocalized('uploadImage'),
                      30 ,onTap),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Container whiteMultilineTextField() {
    return Container(
      decoration: BoxDecoration(
          color: MyColors.colorWhite,
          borderRadius: BorderRadius.circular(8)
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 10,right: 10),
        child: TextField(
          maxLines: 4,
          style: CTheme.textRegular16Black(),
          decoration: InputDecoration(
            border: UnderlineInputBorder(
                borderSide: BorderSide.none
            ),
            fillColor: MyColors.colorWhite,
          ),
        ),
      ),
    );
  }

  Widget profileTypeBox(String headingText,String subText,String btnText
      ,Function onTap)
  {
    return Container(
      decoration: BoxDecoration(
        color: MyColors.appBlue,
        border: Border(
          left: BorderSide(
            color: MyColors.colorLogoOrange,
            width: 5,
          ),
          right: BorderSide(
            color: MyColors.colorLogoOrange,
            width: 5,
          ),
          bottom: BorderSide(
            color: MyColors.colorLogoOrange,
            width: 5,
          ),
          top: BorderSide(
            color: MyColors.colorLogoOrange,
            width: 5,
          ),
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Text(
              headingText,
              style: CTheme.textRegular26LogoOrange(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              height: 0.5,
              width: 200,
              color: MyColors.colorLogoOrange,
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10,left: 75,right: 75),
            child: Text(
              subText,
              style: CTheme.textRegular11WhiteItalic(),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 25,left: 40,right: 40,bottom: 25),
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: onTap,
                      child: Container(
                        alignment: Alignment.center,
                        height: 50,
                        decoration: (
                            BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    Color(0xFF242A37),
                                    Color(0xFF4E586E)
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(1.0, 0.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp
                              ),
                              borderRadius: BorderRadius.circular(10),
                            )
                        ),
                        child: Text(
                          btnText,
                          style: CTheme.textRegularBold18White(),
                        ),
                      ),
                    ),
                  ),
                ],
              )
          )
        ],
      ),
    );
  }

  Widget roundedSquareButton(String btnText,double height,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: height,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText,TextEditingController textEditingController) {
    return TextField(
      controller: textEditingController,
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }

  bool validateFields(){
    var isValidated = false;
    if(imageFile == null){
      CTheme.showAppAlertOneButton(context: context,title: 'Select Image',
          bodyText: 'Please select profile image',btnTitle: 'Okay');
    }
    else if(businessNameTC.text.trim().length==0){
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please specify profile name',btnTitle: 'Okay');
    }
    else if(emailTC.text.trim().length==0){
      CTheme.showAppAlertOneButton(context: context,title: 'Select Profile Type',
          bodyText: 'Please enter business email',
          btnTitle: 'Okay');
    }
    else if(!EmailValidator.validate(emailTC.text.trim())){
      CTheme.showAppAlertOneButton(context: context,title: 'Email Error',
          bodyText: 'Please enter valid email',btnTitle: 'Okay');
    }
    else if(_valueCountry == null)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Select Country',
          bodyText: 'Please select a country',btnTitle: 'Okay');
    }
    else if(_valueCity == null)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Select City',
          bodyText: 'Please select a city',btnTitle: 'Okay');
    }
    ///Hard Coded for testing.
    // else if(_listOfInterestsChipTags.isEmpty){
    //   CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
    //       bodyText: 'Please select interests to proceed',
    //       btnTitle: 'Okay');
    // }
    else{
      isValidated = true;
    }
    return isValidated;
  }
}
  //overridden class to customize chips
class _Chip extends StatelessWidget {
  const _Chip({
    @required this.label,
    @required this.onDeleted,
    @required this.index,
  });

  final String label;
  final ValueChanged<int> onDeleted;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: MyColors.colorLogoOrange,
      labelPadding: const EdgeInsets.only(left: 8.0),
      label: Text(label),
      deleteIcon: Icon(
        Icons.close,
        size: 15,
        color: MyColors.colorWhite,
      ),
      onDeleted: () {
        onDeleted(index);
      },
    );
  }
}