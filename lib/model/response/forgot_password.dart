/// status_code : 200
/// message : "A recovery email has been sent on your email."

class ForgotPassword {
  int _statusCode;
  String _message;

  int get statusCode => _statusCode;
  String get message => _message;

  ForgotPassword({
      int statusCode, 
      String message}){
    _statusCode = statusCode;
    _message = message;
}

  ForgotPassword.fromJson(dynamic json) {
    _statusCode = json["status_code"];
    _message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status_code"] = _statusCode;
    map["message"] = _message;
    return map;
  }

}