
import 'dart:async';
import 'dart:io';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/model/response/cities.dart';
import 'package:app_one/model/response/countries.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/viewmodel/create_user_vmodel.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CreateAccount extends StatefulWidget {

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}


class _CreateAccountScreenState extends State<CreateAccount> {
  var isTermConditionsChecked = false;
  String _valueGender;
  TextEditingController firstNameTC = TextEditingController();
  TextEditingController lastNameTC = TextEditingController();
  TextEditingController emailTC = TextEditingController();
  TextEditingController passwordTC = TextEditingController();
  TextEditingController confirmPasswordTC = TextEditingController();
  DateTime _valueInDTO;
  List<String> listOfCities=[];
  List<String> listOfCountries=[];
  String _valueCountry;
  String _valueCity;
  String _valueDOB ="Date Of Birth";
  bool _passwordVisible;
  bool _confirmPasswordVisible;



  @override
  void initState() {
    _passwordVisible = true;
    _confirmPasswordVisible = true;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return BaseView<CreateUserViewModel>(
      onModelReady: (model) async {
        CTheme.showCircularProgressDialog(context);
        await model.getCountries();
        if(model.countrySuccess)
        {
          print("Successful");
          populateCountryListForDropdown(model.countryData);
          Navigator.pop(context);
          await getCitiesByCountryId(
              model,
              '202');
          setState(() {
            listOfCountries;
            _valueCountry='202-South Africa';
            _valueCity='38028-Alberton';
          });

        }
        else{
          Navigator.pop(context);
          print("Error");
        }
      },
      builder: (context, model, child) => createAccountView(context, model),
    );
  }

  Scaffold createAccountView(BuildContext context,CreateUserViewModel model) {
    return Scaffold(
    backgroundColor: MyColors.appBlue,
    body: BaseScrollView().baseView(context, [

      Padding(
        padding: EdgeInsets.only(top:50,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('firstName'),
        controller: firstNameTC,showHideIcon: false,obscureText: false
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('lastName'),
        controller: lastNameTC,showHideIcon: false,obscureText: false),
      ),

      Padding(
        padding: const EdgeInsets.only(top: 30,left: 40,right: 40),
        child: Container(
          height: 50,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor: MyColors.appBlue),
              child: ButtonTheme(
                child: DropdownButton<String>(
                  underline: Container(
                    color: MyColors.colorWhite,
                    height: 1,
                  ),
                  iconEnabledColor: Colors.white,
                  iconDisabledColor: Colors.white,
                  hint:Text(
                      'Gender',
                      style: CTheme.textRegular18White(),
                      textAlign: TextAlign.start,
                    ),

                  isExpanded: true,
                  items: dropDownMenuItems(),
                  onTap: ()=>FocusScope.of(context).unfocus(),
                  onChanged: (value) {
                    setState(() {
                      _valueGender = value;
                    });
                  },
                  value: _valueGender,
                ),
              ),
            ),
          ),
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top: 20,left: 40,right: 40),
        child: GestureDetector(
          onTap: ()=>{
                FocusScope.of(context).unfocus(),
                dateTimePicker(context)
          },
          child: Container(
            height:40,
            decoration: BoxDecoration(
              color: MyColors.appBlue,
             border: Border(
               bottom: BorderSide(
                 color: MyColors.colorWhite
               )
             )
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 0),
                  child: Text(
                    _valueDOB,
                    style: CTheme.textRegular18White(),
                  ),
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: MyColors.colorWhite,
                  size: 25,
                ),
              ],
            ),
          ),
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('emailAddress'),
          controller: emailTC,showHideIcon: false,obscureText: false
        ),
      ),

      Padding(
        padding: const EdgeInsets.only(top: 30,left: 25,right: 25),
        child: Container(
          height: 50,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor: MyColors.appBlue),
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton<String>(
                  icon: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.arrow_drop_down,
                      size: 25,
                    ),
                  ),
                  underline: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Container(
                      color: MyColors.colorWhite,
                      height: 1,
                    ),
                  ),
                  iconEnabledColor: Colors.white,
                  iconDisabledColor: Colors.white,
                  hint: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Country",
                          style: CTheme.textRegular18White(),
                        ),
                      ),
                    ],
                  ),

                  isExpanded: true,
                  items: listOfCountries.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value.substring(
                                value.indexOf("-") + 1, value.length),
                            style: CTheme.textRegular18White(),
                            textAlign: TextAlign.start,
                          ),
                        );
                      }).toList(),
                      onTap: ()=>FocusScope.of(context).unfocus(),
                      onChanged: (value) {
                        getCitiesByCountryId(
                            model,
                            value.substring(0, value.indexOf('-')));
                    setState(() {
                      _valueCountry = value;
                    });
                  },
                  value: _valueCountry,
                ),
              ),
            ),
          ),
        ),
      ),

      Padding(
        padding: const EdgeInsets.only(top: 30,left: 25,right: 25),
        child: Container(
          height: 50,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor: MyColors.appBlue),
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton<String>(
                  icon: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.arrow_drop_down,
                      size: 25,
                    ),
                  ),
                  underline: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Container(
                      color: MyColors.colorWhite,
                      height: 1,
                    ),
                  ),
                  iconEnabledColor: Colors.white,
                  iconDisabledColor: Colors.white,
                  hint: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "City",
                          style: CTheme.textRegular18White(),
                        ),
                      ),
                    ],
                  ),

                  isExpanded: true,
                  items: listOfCities.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value.substring(
                            value.indexOf("-") + 1, value.length),
                        style: CTheme.textRegular18White(),
                        textAlign: TextAlign.start,
                      ),
                    );
                  }).toList(),
                  onTap: ()=>FocusScope.of(context).unfocus(),
                  onChanged: (value) {
                    setState(() {
                      _valueCity = value;
                    });
                  },
                  value: _valueCity,
                ),
              ),
            ),
          ),
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('createPassword'),
        controller: passwordTC,showHideIcon: true,obscureText: true,
        passwordVisibility: _passwordVisible),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('confirmPassword'),
          controller: confirmPasswordTC,showHideIcon: true,obscureText: true,
        passwordVisibility: _confirmPasswordVisible),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 30),
        child: acceptAgreementRow(),
      ),

      Padding(
        padding: EdgeInsets.only(top: 30,left: 40,right: 40,bottom: 35),
        child: roundedSquareButton(
            Localization.stLocalized('createAccount'),
                ()=>{
              if(validateFields()){
                Navigator.pushNamed(
                    context,
                    '/create_social_profile',
                arguments: {
                                "name": firstNameTC.text.trim(),
                                "gender": _valueGender,
                                "date_of_birth": _valueDOB,
                                "device_type":
                                    Platform.isIOS ? "Ios" : "Android",
                                "device_token": "TBD",
                                "email": emailTC.text.trim(),
                                "country_id": _valueCountry.substring(
                                    0, _valueCountry.indexOf('-')),
                                "city_id": _valueCity.substring(
                                    0, _valueCity.indexOf('-')),
                                "password": passwordTC.text.trim(),
                                "phone": "",
                                "address": "",
                                "about": "",
                                "profile_name": "",
                                "profile_email":emailTC.text.trim(),
                                "profile_phone": "",
                                "profile_address": "",
                                "profile_website": "",
                                "profile_about": "",
                                "profile_type": "",
                                "profile_status": "",
                                "interest": []
                              })
              }
            }
        ),
      ),

    ])
  );
  }

  Stack acceptAgreementRow() {
    return Stack(
          children: [
            SizedBox(
              height: 35,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Localization.stLocalized('iAccept'),
                    style: CTheme.textRegular14White(),
                  ),
                  GestureDetector(
                    onTap: ()=>{
                      CTheme.showAppAlertOneButton(
                        context: context,
                        title: "Terms and Conditions",
                          bodyText: "Help protect your website and its users "
                              "with clear and fair website terms and conditions."
                              " These terms and conditions for a website set out"
                              " key issues such as acceptable use, privacy,"
                              " cookies, registration and passwords,"
                              " intellectual property, links to other"
                              " sites, termination and disclaimers of"
                              " responsibility. Terms and conditions "
                              "are used and necessary to protect a website"
                              "owner from liability of a user relying on the "
                              "information or the goods provided from the site "
                              "then suffering a loss.\n\nMaking your own terms and "
                              "conditions for your website is hard, not "
                              "impossible, to do. It can take a few hours to "
                              "few days for a person with no legal background "
                              "to make. But worry no more; we are here to help "
                              "you out.\n\nAll you need to do is fill up the blank "
                              "spaces and then you will receive an email with "
                              "your personalized terms and conditions."
                      )
                    },
                    child: Text(
                      Localization.stLocalized('termAndCondition'),
                      style: CTheme.textRegular14Blue(),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Theme(
                  data:ThemeData(
                    unselectedWidgetColor: MyColors.colorWhite,
                  ),
                  child: SizedBox(
                    height: 35,
                    width: 35,
                    child: Checkbox(
                      activeColor: Colors.white,
                        value: isTermConditionsChecked,
                        onChanged: (value) {
                          FocusScope.of(context).unfocus();
                          termsConditionsCb(value);
                        },
                        checkColor: Colors.black, // color of tick Mark
                      ),
                  ),
                ),

              ],
            )
          ],
        );
  }

  TextField plainTextField(String hintText, {TextEditingController controller,
  bool obscureText,bool showHideIcon,bool passwordVisibility}) {
    return TextField(
      controller: controller,
      style: CTheme.textRegular18White(),
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            hintText:hintText,
            hintStyle: CTheme.textRegular18White(),
            suffixIcon: showHideIcon?IconButton(
              icon: Icon(
                // Based on passwordVisible state choose the icon
                passwordVisibility
                    ? Icons.visibility_off
                    : Icons.visibility,
                color: MyColors.colorLogoOrange,
              ),
              onPressed: () {
                // Update the state i.e. toogle the state of passwordVisible variable
                setState(() {
                  passwordVisibility = !passwordVisibility;
                  _passwordVisible = passwordVisibility;
                  _confirmPasswordVisible = passwordVisibility;
                });
              },
            ):Container(width: 1),
          ),
      obscureText: obscureText?passwordVisibility:false,
    );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  List<DropdownMenuItem<String>> dropDownMenuItems() {
    return [
      DropdownMenuItem(
        value: "Male",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(
                "Male",
                style: CTheme.textRegular18White(),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "Female",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(
                "Female",
                style: CTheme.textRegular18White(),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  void termsConditionsCb(bool value) {
    if (isTermConditionsChecked == false) {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isTermConditionsChecked = true;
      });
      CTheme.showAppAlertOneButton(context: context,title:"Terms And Agreement",
      bodyText: "I accept the terms and agreements",
      btnTitle:"Okay");
    } else {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isTermConditionsChecked = false;
      });
    }
  }

  bool validateFields(){
    var isValidated = false;
    if(firstNameTC.text.trim().length==0){
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
      bodyText: 'Please enter first name',btnTitle: 'Okay');
    }
    else if(_valueGender== null)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please select gender',btnTitle: 'Okay');
    }
    else if(_valueDOB == 'Date Of Birth')
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Date Of Birth Missing',
          bodyText: 'Please select date of birth',btnTitle: 'Okay');
    }
    else if(_valueInDTO.isSameOrGreaterThanDate(DateTime.now()))
      {
        CTheme.showAppAlertOneButton(context: context,title: 'Date Of Birth Error',
            bodyText: 'Date of birth cannot be current date or greater',btnTitle: 'Okay');
      }
    else if(emailTC.text.trim().length==0)
    {
        CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
            bodyText: 'Please enter email',btnTitle: 'Okay');
    }
    else if(!EmailValidator.validate(emailTC.text.trim())){
      CTheme.showAppAlertOneButton(context: context,title: 'Email Error',
          bodyText: 'Please enter valid email',btnTitle: 'Okay');
    }
    else if(_valueCountry == null)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Select Country',
          bodyText: 'Please select a country',btnTitle: 'Okay');
    }
    else if(_valueCity == null)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Select City',
          bodyText: 'Please select a city',btnTitle: 'Okay');
    }


    else if(passwordTC.text.trim().length==0)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please enter password',btnTitle: 'Okay');
    }
    else if(passwordTC.text.trim().length<8)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Weak Password',
          bodyText: 'Please enter strong password.\nLength should be equal to 8 atleast',
          btnTitle: 'Okay');
    }
    else if(confirmPasswordTC.text.trim().length==0)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please enter password again to confirm',btnTitle: 'Okay');
    }
    else if(passwordTC.text != confirmPasswordTC.text)
      {
        CTheme.showAppAlertOneButton(context: context,title: 'Unmatched Passwords',
            bodyText: 'Password confirmation failed',btnTitle: 'Okay');
      }
    else if(!isTermConditionsChecked)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Terms And Conditions',
          bodyText: 'Accept terms and conditions',btnTitle: 'Okay');
    }
    else
      {
     isValidated = true;
    }
    return isValidated;
  }

  Future dateTimePicker(BuildContext context){
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              color: Color.fromRGBO(70, 69, 69, 1),
              height: MediaQuery.of(context)
                  .copyWith()
                  .size
                  .height /
                  2.5,
              child:Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.center,
                        height: 42,
                        decoration: BoxDecoration(
                          color: MyColors.colorDarkBlack,
                        ),
                        child: Text(
                          'Done',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      onTap: (){
                        setState((){
                          if(_valueInDTO != null)
                          {
                            String formattedDateLocal = DateFormat('dd -MMM- yyyy')
                                .format(_valueInDTO);
                            _valueDOB= formattedDateLocal;
                          }
                          else
                          {
                            _valueInDTO = DateTime.parse("2019-01-01");
                            String formattedDate = DateFormat('ddMMyyyy')
                                .format(_valueInDTO);
                            String formattedDateLocal = DateFormat('dd -MMM- yyyy')
                                .format(_valueInDTO);
                            _valueDOB= formattedDateLocal;
                          }
                        });
                        Navigator.pop(context);
                      },
                    ),
                  ),

                  Expanded(
                    flex: 4,
                    child: Container(

                      child: CupertinoDatePicker(
                        minimumYear: 1800,
                        maximumYear: DateTime.now().year-1,
                        initialDateTime: DateTime.parse('2019-01-01'),
                        backgroundColor: MyColors.colorWhite,
                        onDateTimeChanged: (DateTime dateTime) => {
                          _valueInDTO= dateTime
                        },
                        mode: CupertinoDatePickerMode.date,
                      ),
                    ),
                  ),

                ],
              )
          );
        }
    );
  }

  void populateCountryListForDropdown(Countries countryData) {
    countryData.data.forEach((element) {
      listOfCountries.add("${element.id}-${element.name}");
    });
  }

  void populateCityListForDropdown(Cities cityData) {
    listOfCities=[];
    cityData.data.forEach((element) {
      listOfCities.add("${element.id}-${element.name}");
    });
    listOfCities.sort((a, b) {
      return a.substring(a.indexOf("-")+1).toLowerCase()
          .compareTo(b.substring(b.indexOf("-")+1).toLowerCase());
    });
    print(listOfCities);
  }

  void getCitiesByCountryId(CreateUserViewModel model,String countryId)async {
    CTheme.showCircularProgressDialog(context);
    await model.getCities(countryId);
    if(model.citySuccess)
    {
      print("Successful");
      populateCityListForDropdown(model.cityData);
      setState(() {
        listOfCities;

      });
      Navigator.pop(context);
    }
    else{
      Navigator.pop(context);
      print("Error");
    }
  }
}


  extension DateOnlyCompare on DateTime {
  bool isSameOrGreaterThanDate(DateTime other) {
    return this.year >= other.year && this.month >= other.month
        && this.day >= other.day;
  }
}