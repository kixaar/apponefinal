
import 'dart:async';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/dialog/custom_dialogs.dart';
import 'package:app_one/viewmodel/forgot_password_vmodel.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}


class _ForgotPasswordScreenState extends State<ForgotPassword> {
  TextEditingController emailTC = TextEditingController();
  TextEditingController forgotPasswordCodeTC = TextEditingController();
  TextEditingController passwordTC=TextEditingController();


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  BaseView<ForgotPasswordViewModel>(
      builder: (context, model, child) => forgotPasswordView(context, model),
    );
  }

  Scaffold forgotPasswordView(BuildContext context,ForgotPasswordViewModel model) {
    return Scaffold(
    backgroundColor: MyColors.appBlue,
    body: BaseScrollView().baseView(context, [

      Padding(
        padding: EdgeInsets.only(top: 100),
        child:Container(
          height: 150,
          width: 150,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: MyColors.colorLogoOrange,
          ),
          child: Text(
            Localization.stLocalized('logo'),
            style: CTheme.textRegular25White(),
          ),
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top:50,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('emailAddress'),
        emailTC),
      ),

      Padding(
        padding: EdgeInsets.only(top: 50,left: 40,right: 40),
        child: roundedSquareButton(Localization.stLocalized('done'),
            ()async{
          if(validate())
            {
              CTheme.showCircularProgressDialog(context);
            await model.forgotPassword({
               "email":emailTC.text.trim()
             });
            if(model.forgotPasswordSuccess)
              {
                Navigator.pop(context);
                CustomDialogs.showResetPasswordDialog(
                  labelText: "Enter Code",
                  labelText2: "Enter New Password",
                  btn1Text: "Cancel",
                  btn2Text: "Reset",
                  btn3Text: "Send Code Again",
                  context: context,
                  controller: forgotPasswordCodeTC,
                  controller2:passwordTC,
                  onResetPressed:()async{
                    if(validatePasswordReset())
                    {
                      resetPassword(model, context);
                        }
                  },
                  onSendAgainPressed: ()async{
                    CTheme.showCircularProgressDialog(context);
                    await model.forgotPassword({
                      "email":emailTC.text.trim()
                    });
                    if(model.forgotPasswordSuccess) Navigator.pop(context);
                    else{
                      Navigator.pop(context);
                      Navigator.pop(context);
                    }
                  }
                );
              }
            else{
              Navigator.pop(context);
              CTheme.showAppAlertOneButton(
                context: context,
                bodyText: 'Error updating password',
                title: "Error Updating",
                btnTitle: 'Okay',
              );
            }
            }
            }),
      ),
    ])
  );
  }

  Future resetPassword(ForgotPasswordViewModel model, BuildContext context) async {
    CTheme.showCircularProgressDialog(context);
           await model.updatePassword({
      "verification_code":
          forgotPasswordCodeTC.text.trim(),
      "email": emailTC.text.trim(),
      "password": passwordTC.text.trim()
    });
    if (model.updatePasswordSuccess) {
      Navigator.pop(context);
      CTheme.showAppAlertOneButton(
          context: context,
          bodyText: 'Password updated successfully',
          title: "Password Updated",
          btnTitle: 'Okay',
          handler2: (handler) {
            Navigator.pushNamedAndRemoveUntil(
                context, '/login', (route) => false);
          });
    } else {
      Navigator.pop(context);
      CTheme.showAppAlertOneButton(
        context: context,
        bodyText: 'Error updating password',
        title: "Error Updating",
        btnTitle: 'Okay',
      );
    }
  }

  bool validate(){
    var isValidated = false;

    if(emailTC.text.trim().length == 0)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please enter email',btnTitle: 'Okay');
    }

    else if(!EmailValidator.validate(emailTC.text.trim())){
      CTheme.showAppAlertOneButton(context: context,title: 'Email Error',
          bodyText: 'Please enter valid email',btnTitle: 'Okay');
    }

    else{
      isValidated = true;
    }
    return isValidated;
  }

  bool validatePasswordReset(){
    var isValidated = false;

    if(forgotPasswordCodeTC.text.trim().length == 0)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please enter code',btnTitle: 'Okay');
    }
    else if(passwordTC.text.trim().length == 0)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Field Empty',
          bodyText: 'Please enter new password',btnTitle: 'Okay');
    }
    else if(passwordTC.text.length<8)
    {
      CTheme.showAppAlertOneButton(context: context,title: 'Weak Password',
          bodyText: 'Please enter strong password.\nLength should be equal to 8 atleast',
          btnTitle: 'Okay');
    }
    else
      {
      isValidated = true;
    }
    return isValidated;
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
                  decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
                  ),
                  child: Text(
                    btnText,
                    style: CTheme.textRegularBold18White(),
                  ),
                ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText,TextEditingController controller) {
    return TextField(
      controller: controller,
      style: CTheme.textRegular18White(),
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            hintText:hintText,
            hintStyle: CTheme.textRegular18White(),

          ),
        );
  }

}