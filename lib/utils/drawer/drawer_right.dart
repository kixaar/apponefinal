import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/globals/globals.dart';
import 'package:app_one/model/response/all_friends.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/viewmodel/drawer_right_vmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerRight extends StatefulWidget{

  final BuildContext context;
  DrawerRight({this.context});

  @override
  DrawerRightState createState() => DrawerRightState();
}

class DrawerRightState extends State<DrawerRight>{
  String userName="";


  @override
  void initState() {
    getUserDateForDrawer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<DrawerRightViewModel>(
      builder: (context, model, child) => drawerRightView(context, model),
    );
  }

  SafeArea drawerRightView(BuildContext context,DrawerRightViewModel model){
    return SafeArea(
    child: Drawer(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: MyColors.colorDarkBlack,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 35,left: 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 70,
                      width: 70,
                      child: CircleAvatar(
                        backgroundColor: MyColors.colorWhite,
                        child: Icon(
                          Icons.person,
                          size: 51,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        children: [
                          Text(
                            userName,
                            style: CTheme.textRegular21White(),
                          ),
                          Text(
                            'Social Account',
                            style: CTheme.textRegular11WhiteItalic(),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.only(top: 50, left: 20),
                child:
                navRightTextIconSocial( 'Social Account',
                    ()async{
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                      Navigator.pushNamed(context, '/view_profile',
                      arguments:
                        {
                          "userData":Data(
                            profileId: int.parse(prefs.getString(Globals.profileId)),
                            profileStatus:'public',
                            profileType: prefs.getString(Globals.profileType),
                          ),
                          "currentUserId":prefs.getString(Globals.profileId)
                        }
                      );
                    },
                    true,false,''),
              ),

              Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: navRightTextIconBusiness('My Business',
                            ()=>{
                        },
                        false,true,
                    model),
                  )
            ],
          ),
        ),
      ),
    ),
  );
  }


  Widget navRightTextIconSocial( String text,Function onTap,bool selectedProfileVisibility,
      bool countVisibility,String count) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Visibility(
                        visible: selectedProfileVisibility,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 5,top: 8
                              ),
                          child: Container(
                            decoration: BoxDecoration(
                                color: MyColors.colorLogoOrange,
                                borderRadius: BorderRadius.circular(15)
                            ),
                            height: 15,
                            width: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20,right: 8,top: 5
                            ,bottom: 5),
                        child: Text(
                          text,
                          style: CTheme.textRegular18White(),
                        ),
                      ),

                    ],
                  ),
                  Visibility(
                    visible: countVisibility,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 15,top: 5),
                          child: Container(
                            height: 20,
                            width: 20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: MyColors.colorRedPlay,
                                borderRadius: BorderRadius.circular(20)
                            ),
                            child: Text(
                              count,
                              style: CTheme.textRegular11White(),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),

    );
  }


  Widget navRightTextIconBusiness( String text,Function onTap,bool selectedProfileVisibility,
      bool countVisibility, DrawerRightViewModel model) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Visibility(
                        visible: selectedProfileVisibility,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 5,top: 8
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                                color: MyColors.colorLogoOrange,
                                borderRadius: BorderRadius.circular(15)
                            ),
                            height: 15,
                            width: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20,right: 8,top: 5
                            ,bottom: 5),
                        child: Text(
                          text,
                          style: CTheme.textRegular18White(),
                        ),
                      ),

                    ],
                  ),
                  FutureBuilder<bool>(
                  future: model.getProfileCounts(),
                  builder: (context,profileCountSnapshot){
                    return   Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 15,top: 5),
                          child: profileCountSnapshot.data!=null
                              ?profileCountSnapshot.data
                              ?Container(
                            height: 20,
                            width: 20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: MyColors.colorRedPlay,
                                borderRadius: BorderRadius.circular(20)
                            ),
                            child: Text(
                    model.profileCountData.data.buisnessCount!=null?
                    model.profileCountData.data.buisnessCount.toString()
                              :"0",
                              style: CTheme.textRegular11White(),
                            ),
                          )
                              :Container()
                              :Container(height: 20,width: 20,
                          child:CTheme.showCircularProgressIndicator(strokeWidget: 2)
                          ),
                        )
                      ],
                    );
                  }
                  ),
                ],
              ),
            ),
          ),
        ],
      ),

    );
  }

  getUserDateForDrawer() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userName = prefs.getString(Globals.userName);
    setState(() {
      userName;
    });
  }
}
