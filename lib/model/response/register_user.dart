/// status_code : 200
/// data : {"id":26,"user_id":16,"name":"first social","email":"firstsocial@gmail.com","profile_type":"social","profile_status":"","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYTNhZDYyMmM3MjFmODlhYjU0ODEzNzdhOWM2YWFkZWNiZjhiMjFiNjhiYTAxYWUzMmRhM2EyM2U3NjlhN2I2YmJmMzNhNTNiM2MwZmFkNGMiLCJpYXQiOjE2MDc1MTgwMzcsIm5iZiI6MTYwNzUxODAzNywiZXhwIjoxNjM5MDU0MDM3LCJzdWIiOiIxNiIsInNjb3BlcyI6W119.ebyK4FZBYRAJ86B6vnR4suNgphtSVJhg1SwW3f-YpjjopZ7oqTucJiUKc41a6_qasem354QnhVLTVhKJmjK8eNMJj0S4f2zqTbjdfl6aivgjE9VQj3APhIPQAw_ttu1BGfkfUaPlulr6As__Cgob36rCjiNnrW-5cOX-5XDb2UDxXy1P-le-h1NCN93YlRsG-9z-UkQeV2JfdYzZjV7ErFRsZASzpgdceou4dwMKF2KtWejybJF4rwPHvHlnlBV-8WmmOx4H2EEOWrPg5hXIcYO6JXT4EDqTYksT5ffvrSnJ7J04xCbuvIvDu5nqpkW_Tm9w7bPiFyjvAJTc_KlzeriTlKJLREe9_dGRKOJZpxJ5sXVM1Q2XBgkNneqah0yuO_127aSvHgklxEY39LCKDwPSHGNAXwunzLaxKJRjxeMK7wulbT3WczeO3MbFdH_-S-wif0hDBWf6AokMcJfMVV0eMFrLNa3j48_9IUdyuU4PMJtgpl66D2y5us_VmjkvR6fKSxWzih9es2Z2eLgor_26vQwnlZb4ZH6RDZfKsfcUvGr8PQd6n3ZiVw8yfoGOzDWd6MIYe8havSBNDqkGikSLs8b4KbiZRb4zJX_w2JNlzk-L3OEJ2UwTJowko5voTxs0ia0sf23-uw4X8qBvN0LehGyR51BEIzUJTcNQPMc"}

class RegisterUser {
  int _statusCode;
  Data _data;

  int get statusCode => _statusCode;
  Data get data => _data;

  RegisterUser({
      int statusCode, 
      Data data}){
    _statusCode = statusCode;
    _data = data;
}

  RegisterUser.fromJson(dynamic json) {
    _statusCode = json["status_code"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status_code"] = _statusCode;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// id : 26
/// user_id : 16
/// name : "first social"
/// email : "firstsocial@gmail.com"
/// profile_type : "social"
/// profile_status : ""
/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYTNhZDYyMmM3MjFmODlhYjU0ODEzNzdhOWM2YWFkZWNiZjhiMjFiNjhiYTAxYWUzMmRhM2EyM2U3NjlhN2I2YmJmMzNhNTNiM2MwZmFkNGMiLCJpYXQiOjE2MDc1MTgwMzcsIm5iZiI6MTYwNzUxODAzNywiZXhwIjoxNjM5MDU0MDM3LCJzdWIiOiIxNiIsInNjb3BlcyI6W119.ebyK4FZBYRAJ86B6vnR4suNgphtSVJhg1SwW3f-YpjjopZ7oqTucJiUKc41a6_qasem354QnhVLTVhKJmjK8eNMJj0S4f2zqTbjdfl6aivgjE9VQj3APhIPQAw_ttu1BGfkfUaPlulr6As__Cgob36rCjiNnrW-5cOX-5XDb2UDxXy1P-le-h1NCN93YlRsG-9z-UkQeV2JfdYzZjV7ErFRsZASzpgdceou4dwMKF2KtWejybJF4rwPHvHlnlBV-8WmmOx4H2EEOWrPg5hXIcYO6JXT4EDqTYksT5ffvrSnJ7J04xCbuvIvDu5nqpkW_Tm9w7bPiFyjvAJTc_KlzeriTlKJLREe9_dGRKOJZpxJ5sXVM1Q2XBgkNneqah0yuO_127aSvHgklxEY39LCKDwPSHGNAXwunzLaxKJRjxeMK7wulbT3WczeO3MbFdH_-S-wif0hDBWf6AokMcJfMVV0eMFrLNa3j48_9IUdyuU4PMJtgpl66D2y5us_VmjkvR6fKSxWzih9es2Z2eLgor_26vQwnlZb4ZH6RDZfKsfcUvGr8PQd6n3ZiVw8yfoGOzDWd6MIYe8havSBNDqkGikSLs8b4KbiZRb4zJX_w2JNlzk-L3OEJ2UwTJowko5voTxs0ia0sf23-uw4X8qBvN0LehGyR51BEIzUJTcNQPMc"

class Data {
  int _id;
  int _userId;
  String _name;
  String _email;
  String _profileType;
  String _profileStatus;
  String _token;

  int get id => _id;
  int get userId => _userId;
  String get name => _name;
  String get email => _email;
  String get profileType => _profileType;
  String get profileStatus => _profileStatus;
  String get token => _token;

  Data({
      int id, 
      int userId, 
      String name, 
      String email, 
      String profileType, 
      String profileStatus, 
      String token}){
    _id = id;
    _userId = userId;
    _name = name;
    _email = email;
    _profileType = profileType;
    _profileStatus = profileStatus;
    _token = token;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _userId = json["user_id"];
    _name = json["name"];
    _email = json["email"];
    _profileType = json["profile_type"];
    _profileStatus = json["profile_status"];
    _token = json["token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user_id"] = _userId;
    map["name"] = _name;
    map["email"] = _email;
    map["profile_type"] = _profileType;
    map["profile_status"] = _profileStatus;
    map["token"] = _token;
    return map;
  }

}