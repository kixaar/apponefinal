
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/dialog/AlertBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_tag_editor/tag_editor.dart';

class CreateVideoPost extends StatefulWidget {

  @override
  _UploadVideoScreenState createState() => _UploadVideoScreenState();
}


class _UploadVideoScreenState extends State<CreateVideoPost> {
  var radioValue;

  List<String> _listChipTags = [];

  var dropDownValue;

  var _listForDropdown =['this','that','what'];



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.colorDarkBlack,
        body: BaseScrollView().baseView(context, [

          Padding(
            padding: const EdgeInsets.only(top: 40,left: 50,right: 50),
            child: plainTextField(Localization.stLocalized('title')),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: Text(
              Localization.stLocalized('description'),
              style: CTheme.textRegular21White(),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 30,left:50,right: 50 ),
            child: whiteMultilineTextField(),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(
              Localization.stLocalized('selectCategories'),
              style: CTheme.textRegular21White(),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top:15,left: 40,right: 40,bottom: 30),
            child:Container(
              decoration: BoxDecoration(
                  color: MyColors.colorWhite,
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: TagEditor(
                  length: _listChipTags.length,
                  delimeters: [',', ' '],
                  hasAddButton: false,
                  inputDecoration: const InputDecoration(
                    contentPadding:EdgeInsets.only(left: 5) ,
                    border: InputBorder.none,
                    hintText: 'Enter Interests',
                  ),
                  onTagChanged: (newValue) {
                    setState(() {
                      _listChipTags.add(newValue);
                    });
                  },
                  tagBuilder: (context, index) => _Chip(
                    index: index,
                    label: _listChipTags[index],
                    onDeleted: (index)=>{
                      setState((){
                        _listChipTags.removeAt(index);
                      })
                    },
                  ),
                )
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(
              Localization.stLocalized('selectTopics'),
              style: CTheme.textRegular21White(),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 10, left: 40, right: 40),
            child: Container(
              alignment: Alignment.centerLeft,
              height: 50,
              decoration: BoxDecoration(
                color: MyColors.colorWhite,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                child: Theme(
                  data: Theme.of(context)
                      .copyWith(canvasColor: MyColors.colorWhite),
                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: DropdownButton<String>(
                      iconDisabledColor: MyColors.colorFullBlack,
                      iconEnabledColor: MyColors.colorFullBlack,
                      underline: Container(),
                      isExpanded: true,
                      value: dropDownValue,
                      items: _listForDropdown
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            style: CTheme.textRegular16Black(),
                          ),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          dropDownValue = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              Localization.stLocalized('createNewTopic'),
              style: CTheme.textRegular11Grey(),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(
              Localization.stLocalized('hashTags'),
              style: CTheme.textRegular21White(),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top:15,left: 40,right: 40,bottom: 30),
            child:Container(
              decoration: BoxDecoration(
                  color: MyColors.colorWhite,
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TagEditor(
                    length: _listChipTags.length,
                    delimeters: [',', ' '],
                    hasAddButton: false,
                    inputDecoration: const InputDecoration(
                      contentPadding:EdgeInsets.only(left: 5) ,
                      border: InputBorder.none,
                      hintText: 'Enter Interests',
                    ),
                    onTagChanged: (newValue) {
                      setState(() {
                        _listChipTags.add(newValue);
                      });
                    },
                    tagBuilder: (context, index) => _Chip(
                      index: index,
                      label: _listChipTags[index],
                      onDeleted: (index)=>{
                        setState((){
                          _listChipTags.removeAt(index);
                        })
                      },
                    ),
                  )
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 10,left: 15,right: 15),
            child:profileImageItemUpload(
                    ()=>{
                      popUpStayConnected(context)
                }
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 40,left: 50,right: 50,bottom: 30),
            child: roundedSquareButton(Localization
                .stLocalized('upload'),50,
                    ()=>{popUpConfirmPost(context)}),
          )

        ])
    );
  }
  Future<dynamic> popUpStayConnected(BuildContext context){
    var dialog2 =showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          return Padding(
            padding: const EdgeInsets.only(top: 100,bottom: 150),
            child: Material(
              borderRadius: BorderRadius.circular(15),
              color: Colors.transparent,
              child: Stack(
                fit: StackFit.passthrough,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15,right: 15,bottom: 110),
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(20)
                    ),
                    height: 230,
                    child: Image
                        .asset('assets/images/stay_connected/stayConnected.png',
                      fit: BoxFit.fill,),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0, left: 10, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          elevation: 5,
                          color: MyColors.appBlue,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: MyColors.colorWhite,
                                          borderRadius: BorderRadius.circular(20)
                                      ),
                                      height: 40,
                                      width: 40,
                                    ),
                                  ),

                                  Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('EDGARS',style: CTheme.textRegular18White(),
                                          textAlign: TextAlign.start,),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 0),
                                          child: Text('2 Hours Ago',style: CTheme.textRegular11Grey()),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),



                              Padding(
                                padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                                child: Row(
                                  children: [
                                    Text(
                                      Localization.stLocalized('hashTagText'),
                                      style: CTheme.textRegular14LogoOrange(),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ),

                              Padding(
                                padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                                child: SizedBox(
                                  height: 65,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          child: Text(
                                            Localization.stLocalized('coventryIsACity'),
                                            style: CTheme.textRegular14White(),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),
                                ),

                              ),

                              Padding(
                                padding:EdgeInsets.only(top: 30,left: 15,right: 15,bottom: 20),
                                child: Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.favorite,
                                          color: MyColors.colorWhite,
                                          size: 20,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            '1125',
                                            style:CTheme.textRegular11White(),
                                          ),

                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 25),
                                          child: Icon(
                                            Icons.mode_comment,
                                            color: MyColors.colorWhite,
                                            size: 20,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            '348',
                                            style:CTheme.textRegular11White(),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 25),
                                          child: Icon(
                                            Icons.share,
                                            color: MyColors.colorWhite,
                                            size: 20,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            '115',
                                            style:CTheme.textRegular11White(),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          height: 20,
                                          width: 20,
                                          decoration: BoxDecoration(
                                              color: MyColors.colorWhite,
                                              borderRadius: BorderRadius.circular(30)
                                          ),
                                          child: Icon(
                                            Icons.flag,
                                            color: MyColors.colorRedPlay,
                                            size: 15,
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20,top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: ()=>{
                            Navigator.pop(context)
                          },
                          child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                color: MyColors.appBlue,
                                borderRadius: BorderRadius.circular(20)
                            ),
                            child: Icon(
                              Icons.close,
                              color: MyColors.colorWhite,
                              size: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),


                ],
              ),
            ),
          );
        });
    return dialog2;
  }

  Future<dynamic> popUpConfirmPost(BuildContext context){
    var dialog2 =showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          return Padding(
            padding: const EdgeInsets.only(top: 100,bottom: 50),
            child: Material(
              borderRadius: BorderRadius.circular(15),
              color: MyColors.appBlue,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15,left: 15,right: 15),
                    child: Card(
                      elevation: 5,
                      color: MyColors.appBlue,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: MyColors.colorWhite,
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  height: 40,
                                  width: 40,
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('JohnDoe',style: CTheme.textRegular18White(),
                                      textAlign: TextAlign.start,),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 0),
                                      child: Text('2 Hours Ago',style: CTheme.textRegular11Grey()),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),

                          Container(
                            height: 230,
                            child: Image
                                .asset('assets/images/confirm_post/femaleimage.png',
                              fit: BoxFit.fill,),
                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                            child: Row(
                              children: [
                                Text(
                                  Localization.stLocalized('hashTagText'),
                                  style: CTheme.textRegular14LogoOrange(),
                                  textAlign: TextAlign.start,
                                ),
                              ],
                            ),
                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                            child: SizedBox(
                              height: 65,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Text(
                                        Localization.stLocalized('coventryIsACity'),
                                        style: CTheme.textRegular14White(),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ),

                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 30,left: 15,right: 15,bottom: 20),
                            child: Stack(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: MyColors.colorWhite,
                                      size: 20,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '1125',
                                        style:CTheme.textRegular11White(),
                                      ),

                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 25),
                                      child: Icon(
                                        Icons.mode_comment,
                                        color: MyColors.colorWhite,
                                        size: 20,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '348',
                                        style:CTheme.textRegular11White(),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 25),
                                      child: Icon(
                                        Icons.share,
                                        color: MyColors.colorWhite,
                                        size: 20,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '115',
                                        style:CTheme.textRegular11White(),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: MyColors.colorWhite,
                                          borderRadius: BorderRadius.circular(30)
                                      ),
                                      child: Icon(
                                        Icons.flag,
                                        color: MyColors.colorRedPlay,
                                        size: 15,
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10,right: 10,top: 20,bottom: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                            child: roundedSquareButtonPopup('Edit', 30, null)
                        ),
                        Expanded(
                            child: roundedSquareButtonPopup('Upload', 30,
                                    ()=>{Navigator.pushNamed(context, '/profile_home')})
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          );
        });
    return dialog2;
  }

  Widget roundedSquareButtonPopup(String btnText,double height,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: height,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(

                btnText,

                style: CTheme.textRegular11WhiteBold(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget profileImageItemUpload(Function onTap) {
    return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Card(
                  color: MyColors.appBlue,
                  elevation: 8,
                  shadowColor: MyColors.appBlue,
                  child: SizedBox(
                    height: 220,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 0),
                            child: Padding(
                              padding: const EdgeInsets.all(2),
                              child: Text(
                                Localization.stLocalized('video'),
                                style: CTheme.textRegular25White(),
                                textAlign: TextAlign.center,
                              ),
                            ),

                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15,left: 85,right: 85,bottom: 20),
                          child: roundedSquareButton(Localization
                              .stLocalized('uploadImage'),
                              30 ,onTap),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
  }

  Container whiteMultilineTextField() {
    return Container(
            decoration: BoxDecoration(
              color: MyColors.colorWhite,
              borderRadius: BorderRadius.circular(8)
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 10,right: 10),
              child: TextField(
                maxLines: 4,
                style: CTheme.textRegular16Black(),
                decoration: InputDecoration(
                  border: UnderlineInputBorder(
                    borderSide: BorderSide.none
                  ),
                  fillColor: MyColors.colorWhite,
                ),
              ),
            ),
          );
  }

  Widget profileTypeBox(String headingText,String subText,String btnText
      ,Function onTap)
  {
    return Container(
            decoration: BoxDecoration(
              color: MyColors.appBlue,
              border: Border(
                left: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                right: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                bottom: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
                top: BorderSide(
                  color: MyColors.colorLogoOrange,
                  width: 5,
                ),
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Text(
                    headingText,
                    style: CTheme.textRegular26LogoOrange(),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                      height: 0.5,
                      width: 200,
                      color: MyColors.colorLogoOrange,
                    ),
                  ),

                Padding(
                  padding: const EdgeInsets.only(top:10,left: 75,right: 75),
                  child: Text(
                    subText,
                    style: CTheme.textRegular11WhiteItalic(),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 25,left: 40,right: 40,bottom: 25),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: onTap,
                          child: Container(
                            alignment: Alignment.center,
                            height: 50,
                            decoration: (
                                BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                        Color(0xFF242A37),
                                        Color(0xFF4E586E)
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(1.0, 0.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                )
                            ),
                            child: Text(
                              btnText,
                              style: CTheme.textRegularBold18White(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                )
              ],
            ),
          );
  }
  Widget roundedSquareButton(String btnText,double height,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: height,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }


}
//overridden class to customize chips
class _Chip extends StatelessWidget {
  const _Chip({
    @required this.label,
    @required this.onDeleted,
    @required this.index,
  });

  final String label;
  final ValueChanged<int> onDeleted;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: MyColors.colorLogoOrange,
      labelPadding: const EdgeInsets.only(left: 8.0),
      label: Text(label),
      deleteIcon: Icon(
        Icons.close,
        size: 15,
        color: MyColors.colorWhite,
      ),
      onDeleted: () {
        onDeleted(index);
      },
    );
  }
}